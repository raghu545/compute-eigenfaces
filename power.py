import time

import numpy as np

from base import BaseMethod


class Power(BaseMethod):
    def __init__(self, maxiter= 1e2, tolerance=1e-2):
        """
        Power method.
        """
        BaseMethod.__init__(self, maxiter=maxiter, tolerance=tolerance)

    # def _fit_transform(self,X):
    #     """
    #     Applies Power method to find eigenvalues and eigenvectors
    #     """
    #     global_start_time = time.time()
    #     x = np.random.normal(size=(self.mat_cols))
    #     for i in range(self.maxiter):
    #         start_time = time.time()
    #         newx = X.dot(x)
    #         norm = np.linalg.norm(newx)
    #         self._eigu.append((np.array(x.T.dot(newx))/(x.T.dot(x))).reshape(1,1))
    #         # self._eigu.append((np.array(x.T.dot(X))/(x.T.dot(x))).reshape(1,1))
    #         newx /= norm
    #         self._eigv.append(newx.reshape(self.mat_cols,1))
    #         x = newx
    #         end_time = time.time()
    #         self._times.append(end_time - start_time)
    #     global_end_time = time.time()
    #     self.time = global_end_time - global_start_time
    #     self.iterations = self.maxiter
    #     self.eigu = self._eigu[-1]
    #     self.eigv = self._eigv[-1]

    def _fit_transform(self,X):
        """
        Applies Power method to find eigenvalues and eigenvectors
        """
        global_start_time = time.time()
        x = np.random.normal(size=(self.mat_cols)).reshape((self.mat_cols, 1))
        ev = x.T.dot(X.dot(x))
        for i in range(self.maxiter):
            start_time = time.time()
            Xv = X.dot(x)
            x_new = Xv / np.linalg.norm(Xv)
            ev_new = x_new.T.dot(X.dot(x_new))
            self._eigu.append(np.array(ev_new).reshape(1,1))
            self._eigv.append(x_new.reshape(self.mat_cols,1))
            end_time = time.time()
            self._times.append(end_time - start_time)
            if np.abs(ev - ev_new) < self.tolerance:            
                break
            x = x_new
            ev = ev_new
        # print(f"FINISHED. i {i}, leneigu: {len(self._eigu)}, leneiguv: {len(self._eigv)}, lentimes: {len(self._times)}")
        global_end_time = time.time()
        self.time = global_end_time - global_start_time
        self.iterations = [i + 1]
        self.tot_iterations = i + 1
        self.eigu = self._eigu[-1]
        self.eigv = self._eigv[-1]