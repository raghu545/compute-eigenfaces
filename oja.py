import numpy as np
import math
import time
import base

class Oja(base.BaseMethod):
    def __init__(self, learning_rate=1e-6, maxiter= 1e2, tolerance=1e-2):
        """
        learning_rate: should be lower the more data points htere are
        maxiter: maximum number of iterations, used as stop criteria
        """
        base.BaseMethod.__init__(self, maxiter=maxiter, tolerance=tolerance)
        self.learning_rate = learning_rate

    def _fit_transform(self,X):
        """
        Applies Oja's rule to find 1st principal component of data in X (rows).
        """
        #randomly initialize weights
        weights = np.random.normal(scale=0.25, size=(self.mat_cols, 1))
        if self.debug:
            print("Starting iterations for Oja rule...")
        Ysquare = np.square(X.dot(weights))
        global_start_time = time.time()
        for i in range(self.maxiter):
            start_time = time.time()
            prev_weights = weights.copy()
            Y = np.dot(X, weights)
            prev_ysquaremean = Ysquare.mean()
            Ysquare = np.square(Y)
            weights += self.learning_rate * np.sum(Y*X - Ysquare*weights.T, axis=0).reshape((self.mat_cols, 1))
            residue = np.linalg.norm(weights - prev_weights)
            if self.debug:
                print(f"{i}: {residue}")
            self._eigu.append(np.array(Ysquare.mean()).reshape(1,1))
            self._eigv.append(weights.copy())
            end_time = time.time()
            self._times.append(end_time - start_time)
            if i> 0 and np.abs(Ysquare.mean() - prev_ysquaremean) < self.tolerance:
                break 
        self.ystar = np.dot(X,weights)
        global_end_time = time.time()
        self.time = global_end_time - global_start_time
        self.iterations = [i+1]
        self.tot_iterations = i + 1
        self.eigv = weights
        self.eigu = np.array(Ysquare.mean()).reshape(1,1)
        
