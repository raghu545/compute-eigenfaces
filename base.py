import numpy as np
import time
import datetime as dt
import pandas as pd


class BaseMethod():
    def __init__(self, **kargs):
        if 'maxiter' in kargs:
            self.maxiter = int(kargs['maxiter'])
        else:
            self.maxiter = None
        if 'tolerance' in kargs:
            self.tolerance = kargs['tolerance']
        else:
            self.tolerance = None
        self._eigu = []  # list with computed eigenvalues, one per iteration
        self._eigv = []  # list with computed eigenvectors, one per iteration
        self.eigu = None  # computed eigenvalues
        self.eigv = None  # computed eigenvector matrix
        self.exact_eigu = None
        self.exact_eigv = None
        self._times = []
        self.time = None
        self.iterations = []
        self.tot_iterations = 0
        self.debug = False
        self.reorder = True
        self.normalize_eigv = False
        self.save_data_for_all_iterations = True

        # set this to True to check the found eigenvalues against numpy's np.linalg.eig - this can be SLOW
        self.check_result = False

    def fit_transform(self, X):
        self.mat_rows, self.mat_cols = X.shape
        self._fit_transform(X)
        self.n_computed_eigu = len(self.eigu)
        if self.normalize_eigv:
            eigvs = self.eigv
            for idx, eigv in enumerate(eigvs.T):
                norm = np.linalg.norm(eigv)
                self.eigv[:, idx] = eigv/norm
        assert self.n_computed_eigu == self.eigv.shape[1]
        if self.n_computed_eigu > 1 and self.reorder:
            eigu_argsort = np.argsort(np.abs(self.eigu))[::-1]
            # self._eigu = [self._eigu[i] for i in eigu_argsort]
            # self._eigv = [self._eigv[i] for i in eigu_argsort]
            self.eigu = self.eigu[eigu_argsort]
            self.eigv = self.eigv.T[eigu_argsort].T
            for it in range(len(self._eigu)):
                eigu = self._eigu[it]
                eigv = self._eigv[it]
                self._eigu[it] = eigu[eigu_argsort]
                self._eigv[it] = eigv.T[eigu_argsort].T

    def _compute_exact(self, X):
        self.exact_eigu, self.exact_eigv = np.linalg.eig(X)
        eigu_argsort = np.argsort(self.exact_eigu)[::-1]
        self.exact_eigu = self.exact_eigu[eigu_argsort]
        self.exact_eigv = self.exact_eigv.T[eigu_argsort].T

    def _check_exact_ordering(self, X):
        rows, cols = self.exact_eigv.shape
        for i in range(cols):
            v = self.exact_eigv[:, i]
            l = self.exact_eigu[i]
            print(f"||Av_{i} - lv_{i}|| = {np.linalg.norm(X.dot(v) - l*v)}")
            # import ipdb
            # ipdb.set_trace()

    def pprint_result(self):
        fmtD = "%20s: %5d\n"
        fmtF = "%20s: %5.10f\n"
        print(f"RESULTS - {dt.datetime.utcnow().isoformat()}")
        print(
            fmtD % ('N. Eigv computed', self.n_computed_eigu),
            fmtD % ('Iterations', self.tot_iterations),
            fmtF % ('Time (tot)', self.time),
            fmtF % ('Time (avg)', np.average(self._times))
        )
        if self.exact_eigu is not None:
            toterr = 0
            totverr = 0
            for i in range(self.n_computed_eigu):
                exu = self.exact_eigu[i]
                eu = self.eigu[i]
                toterr += (exu - eu)**2
                exv = self.exact_eigv[:, i].reshape(self.mat_cols, 1)
                ev = self.eigv[i]
                totverr += np.linalg.norm(exv - ev)**2
            toterr /= np.sum(self.exact_eigu**2)
            totverr /= np.sum([np.linalg.norm(v) **
                              2 for v in self.exact_eigv.T])
            print(
                fmtF % ("Eigu rel. error", np.linalg.norm(toterr)),
                fmtF % ("Eigv rel. error", totverr),
            )

    def get_time_error_df(self, eigidx: int = 0):
        timearr = np.cumsum(self._times).reshape(self.tot_iterations, 1)
        eig_list = [alleigs[eigidx] for alleigs in self._eigu]
        errarr = np.abs(np.array(eig_list) -
                        self.exact_eigu[eigidx]).reshape(self.tot_iterations, 1)
        return pd.DataFrame(np.hstack((timearr, errarr)), columns=['Time (s)', 'Error'])

    def get_cum_time_error_df(self):
        timearr = np.cumsum(self._times).reshape(self.tot_iterations, 1)
        cumerrarr = np.vstack(
            [np.linalg.norm(eigs - self.exact_eigu) for eigs in self._eigu])
        return pd.DataFrame(np.hstack((timearr, cumerrarr)), columns=['Time (s)', 'Error'])

    def compute_eigv_with_inverse_iteration(self, X, neigvs=None):
        import utils
        if neigvs is None or neigvs > self.n_computed_eigu:
            neigvs = self.n_computed_eigu
        start = time.time()
        for idx, eigu in enumerate(self.eigu[:neigvs]):
            print(f"inverse iteration: {idx}/{neigvs}")
            mat = X - eigu*np.eye(self.mat_cols)
            arr = utils.inverse_iteration(
                mat, initial_guess=self.eigv[:, idx], tolerance=1e-12)
            self.eigv[:, idx] = arr.reshape(self.mat_cols,)
        end = time.time()
        print(f"Inverse iterations done in {end-start} seconds")
