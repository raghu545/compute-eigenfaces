import base
import os
import numpy as np
from numpy.core.fromnumeric import size
from skimage.transform import resize
import plotly.express as px
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt

def read_pgm(pgmf):
    """Return a raster of integers from a PGM as a list of lists.

    Taken from: https://stackoverflow.com/questions/35723865/read-a-pgm-file-in-python"""
    assert pgmf.readline() == b'P5\n'
    (width, height) = [int(i) for i in pgmf.readline().split()]
    depth = int(pgmf.readline())
    assert depth <= 255
    raster = []
    for y in range(height):
        row = []
        for y in range(width):
            row.append(ord(pgmf.read(1)))
        raster.append(row)
    return np.array(raster)


def get_all_pgm_images(basepath):
    extension = ".pgm"
    all_files = []
    all_images = []
    for root, dirs, files in os.walk(basepath):
        for file in files:
            if file[-len(extension):] == extension:
                filep = os.path.join(root, file)
                all_files.append(filep)
                with open(filep, 'rb') as f:
                    all_images.append(read_pgm(f))

    shape = all_images[0].shape
    for idx, aim in enumerate(all_images):
        try:
            assert aim.shape == shape
        except AssertionError:
            print(f"Image {idx} has shape {aim.shape}")
    print(f"Read {len(all_images)} images of size {shape}")
    return all_images


def resize_images(image_list, newdith, newheight):
    ret = []
    for aim in image_list:
        resizedimage = resize(aim, (newdith, newheight))
        assert resizedimage.shape == (newdith, newheight)
        ret.append(resizedimage/resizedimage.max())
    print(f"Resized images to {(newdith, newheight)}")
    return ret

def prepare_data_and_cov_matrices(image_list, cov_scaling_factor=1):
    mat = image_list[0].flatten()
    for im in image_list[1:]:
        mat = np.vstack((mat, im.flatten()))

    scaler = StandardScaler(with_std=False)
    # scaler = StandardScaler(with_std=True)
    scaled_mat= scaler.fit_transform(mat)
    covmat = np.cov(scaled_mat.T)*cov_scaling_factor
    print(f"Covariance matrix has size {covmat.shape}")
    return scaled_mat, covmat

def show_flattened_image(arr, shape):
    # fig = px.imshow(arr.reshape(shape), color_continuous_scale="greys")
    # fig.show()  
    plt.imshow(arr.reshape(shape), cmap='gray')
    plt.axis('off')
    fig = plt.figure()
    plt.show()
    return fig

def project_vector_on_columns(vector, matrix):
    ret = np.zeros_like(vector)
    for col in matrix.T:
        # print(col.shape, ret.shape)
        scalprod = np.dot(col, vector)
        ret += scalprod*col
    return ret

def inverse_iteration(matrix, initial_guess = None, maxiter=1e2, tolerance=1e-7):
    invmat = np.linalg.inv(matrix)
    n = invmat.shape[1]
    if initial_guess is None:
        b = np.random.normal(size=(n,1))
    else:
        b = initial_guess
    for i in range(int(maxiter)):
        prevb = b.copy()
        b = invmat.dot(b)
        b /= np.linalg.norm(b)
        if np.linalg.norm(prevb - b) < tolerance:
            break
    # print(f"inverse iteration finished in {i} iterations")
    return b

def check_eigv_errors(model: base.BaseMethod, matrix, eigidx: int):
    eigv = model.eigv[:,eigidx]
    eigu = model.eigu[eigidx]

    ex_eigv = model.exact_eigv[:,eigidx]
    ex_eigu = model.exact_eigu[eigidx]

    # Av = randmat.dot(eigv)
    # lv = eigu*eigv
    n = matrix.shape[0]
    AminusL = (matrix - eigu*np.eye(n))
    # AminusL = (randmat - ex_eigu*np.eye(n))
    print(f"detAminusL: {np.linalg.det(AminusL)}")
    linsys_eigv = np.linalg.solve(AminusL, np.zeros(n))
    print(f"linsys eigv: {linsys_eigv}")
    inveigv = inverse_iteration(AminusL, maxiter=100, tolerance=1e-17)
    print(f"linsys eigv: {inveigv}")

    eiguerr = np.abs(eigu - ex_eigu)
    eiguerr /= np.abs(ex_eigu)
    print(f"eigu: {eiguerr}")
    # \njacobi: {jacerr}\nlinear sys: {linsyserr}\ninv: {inverr}")

    names = ["jacobi", "linsys", "inversion", "exact"]
    eigvs = [eigv, linsys_eigv, inveigv, ex_eigv]
    for idx, eg in enumerate(eigvs):
        name = names[idx]
        err = np.linalg.norm(eg - ex_eigv)
        if err != 0:
            err /= np.linalg.norm(ex_eigv)
        print(f"{name}-eigv norm: {np.linalg.norm(eg)}, error: {err}")

def check_eigv_eigu_diff(model: base.BaseMethod, matrix, eigidx: int):
    eigv = model.eigv[:,eigidx]
    eigu = model.eigu[eigidx]

    ex_eigv = model.exact_eigv[:,eigidx]
    ex_eigu = model.exact_eigu[eigidx]


    n = matrix.shape[0]
    AminusL = (matrix - eigu*np.eye(n))
    # AminusL = (randmat - ex_eigu*np.eye(n))
    print(f"detAminusL: {np.linalg.det(AminusL)}")
    linsys_eigv = np.linalg.solve(AminusL, np.zeros(n))
    print(f"linsys eigv: {linsys_eigv}")
    inveigv = inverse_iteration(AminusL, maxiter=100, tolerance=1e-17)
    print(f"linsys eigv: {inveigv}")

    eiguerr = np.abs(eigu - ex_eigu)
    eiguerr /= np.abs(ex_eigu)
    print(f"eigu: {eiguerr}")
    # \njacobi: {jacerr}\nlinear sys: {linsyserr}\ninv: {inverr}")

    names = ["jacobi", "linsys", "inversion", "exact"]
    eigvs = [eigv, linsys_eigv, inveigv, ex_eigv]
    for idx, eg in enumerate(eigvs):
        name = names[idx]
        Av = matrix.dot(eg)
        # lv = ex_eigu*eg
        lv = eigu*eg
        err = np.linalg.norm(Av - lv)
        print(f"{name} error: {err}")

def isSymmetric(arr: np.array) -> bool:
    return np.allclose(arr, arr.T)