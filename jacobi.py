import time

import numpy as np

from base import BaseMethod


def get_givens_theta(matrix, p, q):
    x_pq = matrix[p, q]
    x_pp = matrix[p, p]
    x_qq = matrix[q, q]
    # theta = np.arctan((2*x_pq)/(x_qq - x_pp))/2
    theta = np.arctan((2.0*x_pq)/(x_pp - x_qq))/2.0
    return theta


def givens_rotation(matrix, k: int, l: int, theta: float):
    n = matrix.shape[0]
    # rotated = matrix.copy()
    rotated = matrix
    ct = np.cos(theta)
    st = np.sin(theta)
    for j in range(n):
        if j != k:
            # rotated[p,j] = matrix[p,j]*ct - matrix[q,j]*st
            rotated[k, j] = matrix[j, k]*ct + matrix[j, l]*st
            rotated[j, k] = rotated[k, j]
        if j != l:
            # rotated[q,j] = matrix[q,j]*st + matrix[q,j]*ct
            rotated[l, j] = - matrix[j, k]*st + matrix[j, l]*ct
            rotated[j, l] = rotated[l, j]
    # rotated[p,p] = matrix[p,p]*(ct**2) + matrix[q,q]*(st**2) - matrix[p,q]*np.sin(2*theta)
    rotated[k, k] = matrix[k, k] * \
        (ct**2) + matrix[l, l]*(st**2) + 2*matrix[k, l]*st*ct
    # rotated[q,q] = matrix[p,p]*(st**2) + matrix[q,q]*(ct**2) + matrix[p,q]*np.sin(2*theta)
    rotated[l, l] = matrix[k, k] * \
        (st**2) + matrix[l, l]*(ct**2) - 2*matrix[k, l]*st*ct
    # rotated[p,q] = matrix[p,q]*np.cos(2*theta) + ((matrix[p,p]-matrix[q,q])/2)*np.sin(2*theta)
    # rotated[k,l] = st*ct*(matrix[l,l] - matrix[k,k]) + matrix[k,l]*((ct**2) - (st**2))
    rotated[k, l] = 0
    rotated[l, k]= rotated[k, l]
    return rotated

class Jacobi(BaseMethod):
    def __init__(self, maxiter= 1e2, tolerance=1e-2):
        """
        Jacobi's method.
        """
        BaseMethod.__init__(self, maxiter=maxiter, tolerance = tolerance)

    # @profile
    def _fit_transform(self, X):
        """
        Applies Jacobi's method to find eigenvalues and eigenvectors
        """
        workingX= X.copy()
        # Qmats = []
        self.pq_tuples= []
        self.theta_list= []
        global_start_time= time.time()
        for i in range(self.maxiter):
            if self.debug and i % 100 == 0:
            # if self.debug:
                print(f"Iteration {i}/{self.maxiter}")
            start_time= time.time()
            max_val= -1
            max_p= -1
            max_q= -1
            for p in range(self.mat_rows):
                for q in range(p+1, self.mat_cols):
                    # max_elem= np.abs(workingX[p, q])
                    max_elem= abs(workingX[p, q])
                    if max_elem > max_val:
                        max_val= max_elem
                        max_p= p
                        max_q= q
            assert max_p != max_q
            if max_p > max_q:
                temp= max_p
                max_p= max_q
                max_q= temp
            assert max_p < max_q
            self.pq_tuples.append((max_p, max_q))
            theta= get_givens_theta(workingX, max_p, max_q)
            self.theta_list.append(theta)
            # if max_elem < self.tolerance:
            #     if self.save_data_for_all_iterations:
            #         self._eigu.append(np.diag(workingX).copy())
            #         end_time= time.time()
            #         self._times.append(end_time - start_time)
            #     break
            # newX = givens_rotation(workingX, max_p, max_q, theta)
            # neweigs = np.diag(newX)
            workingX= givens_rotation(workingX, max_p, max_q, theta)
            # neweigs = np.diag(newX)
            end_time= time.time()
            self._times.append(end_time - start_time)
            if self.save_data_for_all_iterations:
                self._eigu.append(np.diag(workingX).copy())
            # if np.all(np.abs(neweigs - np.diag(workingX)) < self.tolerance):
            #     break
            # workingX = newX.copy()
        global_end_time= time.time()
        self.time= global_end_time - global_start_time
        prevQ= np.eye(self.mat_cols)
        for idx, pqt in enumerate(self.pq_tuples):
            theta= self.theta_list[idx]
            p, q= pqt
            Q= np.eye(self.mat_cols)
            ct= np.cos(theta)
            st= np.sin(theta)
            Q[p, p]= ct
            Q[p, q]= -st
            Q[q, p]= st
            Q[q, q]= ct
            newQ = prevQ.dot(Q)
            if self.save_data_for_all_iterations:
                self._eigv.append(newQ)
            # self._eigv.append(Q.dot(prevQ))
            prevQ= newQ.copy()
        self.iterations= [i + 1 for k in range(self.mat_cols)]
        self.tot_iterations= i + 1
        if self.save_data_for_all_iterations:
            self.eigu= self._eigu[-1]
            self.eigv= self._eigv[-1]
        else:
            self.eigu= np.diag(workingX)
            self.eigv= prevQ

    # module jacobi
    # ''' lam,x = jacobi(a,tol = 1.0e-9).
    #     Solution of std. eigenvalue problem [a]{x} = lam{x}
    #     by Jacobi's method. Returns eigenvalues in vector {lam}
    #     and the eigenvectors as columns of matrix [x].
    # '''


    # def jacobi(a,tol = 1.0e-9): # Jacobi method
    def _fit_transform_(self, X):  # Jacobi method
        workingX= X.copy()
        def maxElem(a):  # Find largest off-diag. element a[k,l]
            n= len(a)
            aMax= 0.0
            for i in range(n-1):
                for j in range(i+1, n):
                    if abs(a[i, j]) >= aMax:
                        aMax= abs(a[i, j])
                        k= i; l = j
            return aMax, k, l

        def rotate(a, p, k, l):  # Rotate to make a[k,l] = 0
            n= len(a)
            aDiff= a[l, l] - a[k, k]
            if abs(a[k, l]) < abs(aDiff)*1.0e-36: t= a[k, l]/aDiff
            else:
                phi= aDiff/(2.0*a[k, l])
                t= 1.0/(abs(phi) + np.sqrt(phi**2 + 1.0))
                if phi < 0.0: t= -t
            c= 1.0/np.sqrt(t**2 + 1.0); s = t*c
            tau= s/(1.0 + c)
            temp= a[k, l]
            a[k, l]= 0.0
            a[k, k]= a[k, k] - t*temp
            a[l, l]= a[l, l] + t*temp
            for i in range(k):      # Case of i < k
                temp= a[i, k]
                a[i, k]= temp - s*(a[i, l] + tau*temp)
                a[i, l] = a[i, l] + s*(temp - tau*a[i, l])
            for i in range(k+1, l):  # Case of k < i < l
                temp= a[k, i]
                a[k, i] = temp - s*(a[i, l] + tau*a[k, i])
                a[i, l] = a[i, l] + s*(temp - tau*a[i, l])
            for i in range(l+1, n):  # Case of i > l
                temp= a[k, i]
                a[k, i]= temp - s*(a[l, i] + tau*temp)
                a[l, i] = a[l, i] + s*(temp - tau*a[l, i])
            for i in range(n):      # Update transformation matrix
                temp= p[i, k]
                p[i, k] = temp - s*(p[i, l] + tau*p[i, k])
                p[i, l] = p[i, l] + s*(temp - tau*p[i, l])

        global_start_time= time.time()
        n= self.mat_cols
        p= np.identity(n)*1.0     # Initialize transformation matrix
        for i in range(self.maxiter):  # Jacobi rotation loop
            start= time.time()
            aMax, k, l= maxElem(workingX)
            if self.save_data_for_all_iterations:
                self._eigu.append(np.diagonal(workingX).copy())
                self._eigv.append(p.copy())
            if self.debug:
                print(f"Iteration {i}/{self.maxiter} ({aMax})")
            if aMax < self.tolerance:
                #  return np.diagonal(X),p
                end= time.time()
                self._times.append(end - start)
                break
            rotate(workingX, p, k, l)
            end= time.time()
            self._times.append(end - start)
        global_end_time= time.time()
        self.time= global_end_time - global_start_time
        self.iterations= [i + 1 for k in range(self.mat_cols)]
        self.tot_iterations= i + 1
        if self.save_data_for_all_iterations:
            self.eigu= self._eigu[-1]
            self.eigv= self._eigv[-1]
        else:
            self.eigu= np.diagonal(workingX)
            self.eigv= p
