we present four different methods to compute eigenvalues, each with its own characteristics, strengths and weaknesses. After formally introducing the methods we use them in various numerical experiments to test speed
of convergence, stability as well as performance when used to compute eigenfaces.
