import numpy as np
import math
from sklearn.datasets import make_blobs
from sklearn.preprocessing import StandardScaler
import time
from base import BaseMethod
from oja import Oja

class OjaSanger(BaseMethod):
    def __init__(self, learning_rate=1e-6, maxiter=1e5, tolerance=1e-2, compute_n_eigs=None):
        """
        learning_rate: should be lower the more data points htere are
        """
        BaseMethod.__init__(self, maxiter=maxiter, tolerance=tolerance)
        self.learning_rate = learning_rate
        self.reorder = False
        self.compute_n_eigs = compute_n_eigs

    # def _oja(self, X, learning_rate):
    #     """
    #     Apply Oja rule 
    #     """
    #     if self.debug:
    #         print(f"Starting {self.iterations} iterations of Oja's rule")
    #     weights = np.random.normal(scale=0.25, size=(self.mat_cols, 1))
    #     for i in range(self.maxiter):
    #         prev_weights = weights.copy()
    #         Y = np.dot(X, weights)
    #         Ysquare = np.square(Y)
    #         weights += learning_rate * np.sum(Y*X - Ysquare*weights.T, axis=0).reshape((self.mat_cols, 1))
    #         residue = np.linalg.norm(weights - prev_weights)
    #         if math.isnan(residue) or math.isnan(Y.any()):
    #             raise Exception("Some entries are NaN. Try setting a smaller learning rate.")
    #     Ystar = np.dot(X,weights)
    #     return Ystar, weights, np.square(Ystar).mean()

    def _fit_transform(self,X):
        """
        Applies Oja-Sanger's rule to find eigenvectors and eigenvalues of covariance matrix of X.
        """
        if self.compute_n_eigs is None:
            self.compute_n_eigs = self.mat_cols
        if self.check_result:
            print(f"Computing correct eigenvectors...")
            correct_eigv, correct_eigu = self._check(X)
        workingX = X.copy()
        # eigu_list = [np.zeros(self.mat_cols) for i in range(self.mat_cols*self.maxiter)]
        # eigv_list = [np.zeros((self.mat_cols,self.mat_cols)) for i in range(self.mat_cols*self.maxiter)]
        eigu_list = []
        eigv_list = []
        global_start_time = time.time()
        for i in range(self.compute_n_eigs): #i represents eigenvalue index
            print(f"Computing eigenvector {i}/{self.mat_cols - 1}...")
            # ystar, eigv, eigu = self._oja(workingX, self.learning_rate)
            oja = Oja(learning_rate=self.learning_rate,maxiter=self.maxiter,tolerance=self.tolerance)
            oja.fit_transform(workingX)
            start_time = time.time()
            if i == 0:
                self.iterations.append(oja.tot_iterations)
            else:
                self.iterations.append(self.iterations[-1] + oja.tot_iterations)
            ystar = oja.ystar
            eigv = oja.eigv
            eigu = oja.eigu
            if len(eigu_list) == 0:
                prev_eigus = np.zeros(self.compute_n_eigs)
                prev_eigvs = np.zeros((self.mat_cols, self.compute_n_eigs))
            else:
                prev_eigus = eigu_list[-1].copy() #array of prev eigus approx
                prev_eigvs = eigv_list[-1].copy()
            for it in range(oja.tot_iterations):
                prev_eigus[i] = oja._eigu[it]
                eigu_list.append(prev_eigus.copy())
                prev_eigvs[:,i] = oja._eigv[it].reshape(self.mat_cols,)
                eigv_list.append(prev_eigvs.copy())
                # oja_eigu = oja._eigu[it]
                # offset = self.maxiter*i
                # eigu_list[offset + it][i] = oja_eigu
                # oja_eigv = oja._eigv[it]
                # eigv_list[offset + it][:,i] = oja_eigv.reshape(self.mat_cols,)
                # for previ in range(i):
                #     prevlast = offset - 1
                #     eigu_list[offset + it][previ] = eigu_list[prevlast][previ]
                #     eigv_list[offset + it][:,previ] = eigv_list[prevlast][:,previ]
            for rowIdx, row in enumerate(workingX):
                row = row.reshape((self.mat_cols,1))
                eigvSubspace = float(ystar[rowIdx])*eigv
                x1 = row - eigvSubspace
                workingX[rowIdx] = x1.reshape((1,self.mat_cols))
            end_time = time.time()
            oja_times = oja._times
            oja_times[-1] += end_time - start_time
            # self._times.append(end_time - start_time)
            self._times += oja_times
        global_end_time = time.time()
        # self.iterations = len(self._times)
        self.tot_iterations = self.iterations[-1]
        self.time = global_end_time - global_start_time
        self.eigu = eigu_list[-1]
        self.eigv = eigv_list[-1]
        self._eigu = eigu_list
        self._eigv = eigv_list
